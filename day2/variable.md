# 变量

## 变量类型

| 类型 | 默认值 |  
| :---: | :---: |
| bool | true |
| string | 空字符串 |
| int | 0 |
| int8 | 0 |
| int16 | 0 |
| int32 | 0 |
| int64 | 0 |
| uint | 0 |
| uint8 | 0 |
| uint16 | 0 |
| uint32 | 0 |
| uint64 | 0 |
| uintptr | 0 |
| byte | 0 |
| rune | 0 |
| float32 | 0.0 |
| float64 | 0.0 |
| complex64 | (0+0i) |
| complex128 | (0+0i) | 
| 指针 | nil | 

## 声明变量

### 标准格式  

    var 变量名 变量类型 ,例如：  
    func defineVar() int {
	    var a int = 100
	    return a
    }

### 批量格式

    var (
        a int
        b string
        c []float32
        d func() bool
        e struct {
            x int
        }
    )

### 简短格式

    名字 := 表达式
    func defineSmall() int {
	    i, j := 0, 1
	    return i + j
    }


## 变量初始化

### 标准格式

    var 变量名 类型 = 表达式  
    var hp int = 100

### 编译器推导

    在标准格式的基础上，编译器会尝试根据等号右边的表达式推导变量的类型。
    var 变量名 = 表达式  
    var attack = 40
    var defence = 20
    var damageRate float32 = 0.17
    var damage = float32(attack-defence) * damageRate
    fmt.Println(damage)

### 短变量初始化

    变量名 := 表达式
    hp := 100

    *如果 hp 已经被声明过，但依然使用:=时编译器会报错，代码如下：
    // 声明 hp 变量
    var hp int
    // 再次声明并赋值
    hp := 10
    编译报错如下：
    no new variables on left side of :=
    意思是，在“:=”的左边没有新变量出现，意思就是“:=”的左边变量已经被声明了。

    正确写法：
    var hp int
    hp = 100

## 多变量同时赋值

    var a int = 100
    var b int = 200
    b, a = a, b

## 匿名变量

    匿名变量的特点是一个下画线“_”，“_”本身就是一个特殊的标识符，被称为空白标识符。  
    它可以像其他标识符那样用于变量的声明或赋值（任何类型都可以赋值给它），但任何赋  
    给这个标识符的值都将被抛弃，因此这些值不能在后续的代码中使用，也不可以使用这个  
    标识符作为变量对其它变量进行赋值或运算。使用匿名变量时，只需要在变量声明的地方  
    使用下画线替换即可。例如：  
    func GetData() (int, int) {
        return 100, 200
    }
    func main(){
        a, _ := GetData()
        _, b := GetData()
        fmt.Println(a, b)
    }  
    * 匿名变量不占用内存空间，不会分配内存。匿名变量与匿名变量之间也不会因为多次声明而无法使用。  