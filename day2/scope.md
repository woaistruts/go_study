# 变量作用域

## 局部变量

1. 在函数体内声明的变量称之为局部变量，它们的作用域只在函数体内，函数的参数和返回值变量都属于局部变量。
2.  局部变量不是一直存在的，它只在定义它的函数被调用后存在，函数调用结束后这个局部变量就会被销毁。

## 全局变量

1.  在函数体外声明的变量称之为全局变量，全局变量只需要在一个源文件中定义，就可以在所有源文件中使用，当然，不包含这个全局变量的源文件需要使用“import”关键字引入全局变量所在的源文件之后才能使用这个全局变量。
2.  全局变量声明必须以 var 关键字开头，如果想要在外部包中使用全局变量的首字母必须大写。
3.  Go语言程序中全局变量与局部变量名称可以相同，但是函数体内的局部变量会被优先考虑。

## 形式参数

1.  在定义函数时函数名后面括号中的变量叫做形式参数（简称形参）。形式参数只在函数调用时才会生效，函数调用结束后就会被销毁，在函数未被调用时，函数的形参并不占用实际的存储单元，也没有实际值。
2.  形式参数会作为函数的局部变量来使用。