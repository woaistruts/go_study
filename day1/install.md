# 环境

* ## 安装go

    `brew install go`,执行效果如下（因为以前安装过，如果不一致忽略）:
    ![安装过程](WX20210826-165350@2x.png)

* ## 验证安装
    
    `go version`,执行效果如下:
    ![验证安装](WX20210826-170001@2x.png)
    `go env`，执行效果如下:
    ![环境验证](WX20210826-171141@2x.png),如果没有GOPATH，编辑~/.bash_profile文件:  
    `export GOPATH=/Users/username/go`  
    `export GOBIN=$GOPATH/bin`  
    `export PATH=$PATH:$GOBIN`  

* ## 安装vscode
  
    官网下载，或者`brew cask install visual-studio-code`

* ## 安装vscode go插件
  
    ![go插件](WX20210826-190523@2x.png)
    cmd+shift+p,输入`go::install`，全选点击ok