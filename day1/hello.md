* hello world

* ## 创建第一个项目
  
  `mkdir -p ~/dev/go_study`,用vscode打开文件夹,执行`go mod init study`，发现目录下多了一个go.mod。  

  创建hello.go文件,在文件中写入如下代码：
  ```
    package main
    import "fmt"
    func hello() {
	    fmt.Println("hello world!")
    }
  ```  

    命令行运行`go run hello.go`,输出`hello world`

* ## 单元测试
    
    由于一个包下只能有一个main函数(影响build，命令行运行单个go文件不受影响)，所以不采用main的情况下，采用单元测试来继续学习。

    1.  创建一个stu01包,编写stu01.go文件，内容如下：  
        ```
            package stu01
            import "fmt"
            func hello() {
                fmt.Println("123")
            }
        ```  
        ps:在package main的同目录下创建该文件，一直报错，具体为什么以后学习包管理时再议。
    2.  在stu01目录下创建stu01_test.go，内容如下：  
        ```
            package stu01_test
            import "testing"
            func TestHello(t *testing.T) {
                t.Log("success")
            }
        ```  
        ps:在方法上面提示了run test，点击后查看运行状况，发现成功了，毛线输出没有。配置test输出，方法如下：
        ![配置test](WX20210827-111305@2x.png)
        在json内补充`["-v","-count=1"]`，输出日志并清理缓存，执行run test。同时也可以在stu01目录内命令行执行`go test -v -count=1 stu01_test.go`
