# Summary

* [介绍](README.md)
* day1
    - [环境](day1/install.md)
    - [hello world](day1/hello.md)
* day2
    - [变量](day2/variable.md)
    - [作用域](day2/scope.md)